import unittest
from herencia import Paciente, Medico

class TestPaciente(unittest.TestCase):
    def setUp(self):
        self.paciente1 = Paciente("Paula", "González", "10-2-2003", "12345678A", "Historial de Paula González")

    def test_VerHistorialClinico(self):
        self.assertEqual(self.paciente1.VerHistorialClinico(), "Historial de Paula González")

class TestMedico(unittest.TestCase):
    def setUp(self):
        self.medico1 = Medico("Jimena", "Romero", "9-5-1973", "87654321B", "Psicología", "10-11-2024 a las 10am")
    
    def test_ConsultarAgenda(self):
        self.assertEqual(self.medico1.ConsultarAgenda(), "10-11-2024 a las 10am")


if __name__ == '__main__':
    unittest.main()
