class Persona: 
    def __init__ (self, n, a, f, DNI):
        self.nombre = n
        self.apellido = a
        self.fecha_nacimiento = f
        self.identificacion = DNI
    
    def __str__ (self):
        return f"Nombre y apellido: {self.nombre} {self.apellido}. Fecha de nacimiento: {self.fecha_nacimiento}. DNI: {self.identificacion}".format(n=self.nombre, a=self.apellido, f=self.fecha_nacimiento, DNI=self.identificacion)
    
    def setNombre (self, n):
        self.nombre = n
    
    def getNombre (self):
        return self.nombre 
    
    def setApellido (self, n):
        self.apellido = a
    
    def getApellido (self):
        return self.apellido 
    
    def setFechaNacimiento (self, f):
        self.fecha_nacimiento = f
    
    def getFechaNacimiento (self):
        return self.fecha_nacimiento 

    def setIdentificacion (self, DNI):
        self.identificacion = DNI

    def getIdentificacion (self):
        return self.identificacion 
    
class Paciente (Persona):
    def __init__ (self, n, a, f, DNI, hc):
        super().__init__(n, a, f, DNI)
        self.historial_clinico = hc

    def VerHistorialClinico (self):
        return self.historial_clinico
    

class Medico (Persona):
    def __init__(self, n, a, f, DNI, esp, ct):
        super().__init__(n, a, f, DNI)
        self.especialidad = esp
        self.citas = ct

    def ConsultarAgenda (self):
        return self.citas
    
paciente1 = Paciente("Paula", "González", "10-2-2003", "12345678A", "Historial de Paula González")
medico1 = Medico("Jimena", "Romero", "9-5-1973", "87654321B", "Psicología", "10-11-2024 a las 10am")


print("Información del paciente:")
print(paciente1)
print("Historial clínico:", paciente1.VerHistorialClinico())
print ()
print("Información del médico:")
print(medico1)  
print("Especialidad:", medico1.especialidad)
print(medico1.ConsultarAgenda())
